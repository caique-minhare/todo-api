require 'rails_helper'

RSpec.describe 'Todos API', type: :request do
  # Init test data
  let!(:todos) { create_list(:todo, 10)}
  let(:todo_id) {todos.first.id}

  # Test suite for GET /todos
  describe 'GET /todos' do
    # Make http get request before each example
    before {get '/todos'}
    it 'returns todos' do
      # Note 'json' is a custom helper to parse JSON
      # responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end # End of  describe 'GET /todos'

  # Test suite for GET /todos/:id
  describe 'GET /todos/:id' do
    before {get "/todos/#{todo_id}"}

    context 'when the record exists' do
      it 'returns the todo' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(todo_id)
      end

      it 'return status code 200' do
        expect(response).to have_http_status(200)
      end
    end # End of context 'when the record exists'

    context 'when the record does not exist' do
      let(:todo_id) { 100 }

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Todo/)
      end
    end # End of context 'when the record does not exist'
  end # End of 'GET /todos/:id'

  #Test suite for POST /todos
  describe 'POST /todos' do
    #valid Payload
    let(:valid_attributes){{title: 'Learn Elm', created_by: '1'}}

    context 'when the request is valid' do
      before { post '/todos', params: valid_attributes }

      it 'creates a todo' do
        expect(json['title']).to eq('Learn Elm')
      end

      it 'return status code 201' do
        expect(response).to have_http_status(201)
      end
    end # End of context 'when the request is valid'

    context 'when the request is invalid' do
      before { post '/todos', params: {title: 'Foobar'}}

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
        .to match(/Validation failed: Created by can't be blank/)
      end
    end # End of context 'when the request is invalid'
  end # End of 'POST /todos'

  #Test suite for PUT /todos/:id
  describe 'PUT /todos/:id' do
    let(:valid_attributes) { {title: 'Shopping'} }

    context 'when the record exists' do
      before { put "/todos/#{todo_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end # End of context 'when the record exists'
  end # End of 'PUT /todos/:id'

  # Test suite for DELETE /todos/:id
  describe 'DELETE /todos/:id' do
    before { delete "/todos/#{todo_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end # End of 'DELETE /todos/:id'

end # End of RSpec
